(defproject jukebox "0.1.0-SNAPSHOT"
  :description "groov instances, any version, on demand"

  :dependencies [[org.clojure/clojure "1.10.1"]

                 ;; Timbre is a pure Clojure logging library. I could have use
                 ;; clojure.tools.logging, which maps onto the normal Java
                 ;; options (log4j, slf4j, etc), but I wanted to branch out.
                 ;;
                 ;; Plus the name kind of goes with the theme.
                 [com.taoensso/timbre "4.10.0"]

                 ;; I'm iffy on this: I'm using it to make error handling a bit more functional/monadic,
                 ;; but I'm not convinced it's a good idea in all places.
                 [failjure "1.3.0"]

                 ;; Used to get configuration stuff from the environment.
                 [environ "1.1.0"]

                 ;; Project workflow. I'm using Stuart Sierra's reloaded workflow
                 ;; (http://thinkrelevance.com/blog/2013/06/04/clojure-workflow-reloaded)
                 ;; to manage the application lifecycle.
                 [com.stuartsierra/component "0.4.0"]
                 [reloaded.repl "0.2.4"]

                 ;; Databases! Jukebox stores working data in a SQLite database, and we also need
                 ;; to be able to dig into uploaded projects to make sure they're valid.
                 [org.clojure/java.jdbc "0.7.9"]
                 [org.xerial/sqlite-jdbc "3.28.0"]

                 ;; And Ragtime for creating/maintaining the Jukebox database. It handles migrations.
                 [ragtime "0.8.0"]

                 ;; The web server. I'll probably switch to Jetty at some point, but http-kit is smaller
                 ;; for now and gets up me up and running.
                 [http-kit "2.3.0"]

                 ;; Ring is Clojure's de-facto HTTP server abstraction, like Rack for Ruby.
                 [ring "1.7.1"]

                 ;; Middleware for ring to make it simple to handle JSON stuff.
                 [ring/ring-json "0.4.0"]

                 ;; Using clj-http for proxying requests out to groov instances. httpkit includes an HTTP client,
                 ;; but it's been kind of janky. clj-http is built on top of Apache's HTTP components, and it's
                 ;; rock solid.
                 [clj-http "3.10.0"]

                 ;; JSON encoding and decoding. I'll probably switch to Cheshire later (it's built on Jackson and
                 ;; is much faster), but I'm trying to keep dependencies down for now.
                 [org.clojure/data.json "0.2.6"]

                 ;; For rendering HTML
                 [hiccup "1.0.5"]

                 ;; For routing requests on the server side.
                 [compojure "1.6.1"]

                 ;; FRONTEND STUFF.
                 ;; Need to specify the version of ClojureScript I'm using.
                 [org.clojure/clojurescript "1.10.520"]

                 ;; Re-frame is a state management solution for Reagent. It's basically the
                 ;; framework that the whole application is built-on. This pulls in the necessary
                 ;; Reagent dependency as well.
                 [re-frame "0.10.7"]

                 ;; Plugin for re-frame that adds http requests as a side effect in event handlers.
                 [day8.re-frame/http-fx "0.1.6"]]

  :plugins [;; Used to pipe things from this project file (and an associated profiles.clj) into
            ;; the runtime environment
            [lein-environ "1.1.0"]

            ;; For compiling ClojureScript code.
            [lein-cljsbuild "1.1.7"]

            ;; Provides live-reloading of ClojureScript code in development.
            [lein-figwheel "0.5.19"]]

  ;; These are the normal default source and resource locations, but I like to be explicit
  :source-paths ["src"]
  :resource-paths ["resources"]

  :clean-targets ^{:protect false} ["target"
                                    "resources/public/js/out"
                                    "resources/public/js/jukebox.js"
                                    "resources/public/js/jukebox.js.gz"]

  :repl-options {:init-ns jukebox.backend.dev}

  :main ^:skip-aot jukebox.backend.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}}

  ;; Let Figwheel know where my css is, so it'll automatically reload it if I make any changes.
  :figwheel {:css-dirs ["resources/public/css"]}

  :cljsbuild {:builds {;; Development build, includes support for live-coding using Figwheel
                       :dev {:source-paths ["src"]
                             :figwheel {;; Figwheel can optionally call a function after it hot-loads code. This
                                        ;; lets me hook in, clear re-frame subscriptions, and trigger a re-render.
                                        :on-jsload jukebox.frontend.core/render-root

                                        ;; By default Figwheel makes connections to localhost, which is safer, but
                                        ;; doesn't let me test live on a mobile device.
                                        :websocket-host :js-client-host}
                             :compiler {;; The "main" file for the application. The compiler will start compiling here, and
                                        ;; pull in any dependencies transitively.
                                        :main jukebox.frontend.core

                                        ;; Where to stick intermediate files. While developing, the compiler will generate
                                        ;; one JavaScript file per namespace, and they'll need to be served up by the web
                                        ;; server to load the application, so we stick them in the server's resource path.
                                        :output-dir "resources/public/js/out"

                                        ;; Tells the compiler where in the web server's resource path it'll be able to
                                        ;; find and load those intermediate files from.
                                        :asset-path "js/out"

                                        ;; The place to stick the final compiled output.
                                        :output-to "resources/public/js/jukebox.js"

                                        ;; Prints out some timing statistics while compiling.
                                        :compiler-stats true}}

                       ;; Optimized, production build.
                       :min {:source-paths ["src"]
                             :compiler {:main jukebox.frontend.core
                                        :output-to "resources/public/js/jukebox.js"
                                        :optimizations :advanced
                                        :compiler-stats true
                                        :pretty-print false}}}})

(ns jukebox.frontend.core
  "Entry point for the browser frontend."
  (:require [reagent.core :as reagent]
            [re-frame.core :as rf]

            jukebox.common.specs))

(defn loaded
  []
  [:div.container-fluid
   [:div.row
    [:div.col
     [:h1.display-4 "Loaded."]]]])

(defn render-root
  []
  (rf/clear-subscription-cache!)
  (reagent/render [loaded] (.getElementById js/document "app")))

(defn ^:export main
  []
  (render-root))

(ns jukebox.common.specs
  "Data specifications, used for validating things and generally documenting what's
  flying around my application."
  (:require [clojure.spec.alpha :as s]))

#?(:clj (defn file?
          "Just a quick predicate to test whether something is a java.io.File instance."
          [thing]
          (instance? java.io.File thing)))

;; A particular copy of groov.
(s/def :application/id uuid?)
(s/def :application/file-name string?)
(s/def :application/display-name string?)
(s/def :application/version string?)
(s/def :application/revision int?)
(s/def :application/tech-preview (s/nilable string?))
(s/def :application/build-date inst?)
(s/def :application/fingerprint string?)

#?(:clj (s/def :application/file file?))

(s/def :jukebox/application (s/keys :req [:application/id :application/file-name :application/display-name
                                          :application/version :application/revision :application/build-date
                                          :application/fingerprint
                                          #?@(:clj [:application/file])]
                                    :opt [:application/tech-preview]))

(s/def :project/id uuid?)
(s/def :project/file-name string?)
(s/def :project/schema-version int?)
(s/def :project/minimum-app-version string?)
(s/def :project/fingerprint string?)

#?(:clj (s/def :project/file file?))

(s/def :janet/project (s/keys :req [:project/id :project/file-name :project/schema-version
                                    :project/minimum-app-version
                                    :project/fingerprint #?@(:clj [:project/file])]))


(s/def :process/id uuid?)
(s/def :process/display-name string?)
(s/def :process/port int?)
(s/def :process/launch-time inst?)
(s/def :process/url-slug string?)

#?(:clj (s/def :process/working-dir file?))
#?(:clj (s/def :process/project-dir file?))
#?(:clj (s/def :process/library-dir file?))
#?(:clj (s/def :process/stdout      file?))
#?(:clj (s/def :process/stderr      file?))
#?(:clj (s/def :process/pid #(instance? java.lang.Process %)))

(s/def :process/state #{:process/provisioning
                        :process/provisioning-failed
                        :process/ready
                        :process/starting
                        :process/start-failed
                        :process/running})

(s/def :system/process (s/keys :req [:process/id :process/display-name :process/url-slug :process/state :application/id
                                    #?@(:clj [:process/working-dir :process/project-dir :process/library-dir
                                              :process/stdout :process/stderr])]
                              :opt [:process/port :process/launch-time #?@(:clj [:process/pid])]))

(s/def :system/process-state (s/map-of uuid? :system/process))

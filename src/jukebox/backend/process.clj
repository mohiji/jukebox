(ns jukebox.backend.process
  "A process is a single instance of a groov application + optional project that users can create and run."
  (:require [clojure.string :as string]
            [clojure.java.io :as io]
            [taoensso.timbre :as log]
            [failjure.core :as f]

            [jukebox.backend.files :as files])
  (:import [java.io File PrintWriter]
           [java.nio.file Files StandardCopyOption]
           [java.nio.file.attribute FileAttribute]
           [java.time Instant]
           [java.util Date]
           [java.util.zip ZipInputStream ZipEntry]
           [java.util.concurrent TimeUnit]))

;; Just a shorthand for the same sort of copy options we'll be using all over the place.
(def copy-options (into-array StandardCopyOption [StandardCopyOption/REPLACE_EXISTING]))

;; groov View doesn't bundle a copy of Jetty (yet), it expects to be deployed on an existing application
;; server. I don't go that route here: I just fake things out to make it look like groov View runs an
;; embedded copy of Jetty. To do that, I provide the Jetty jars along with a small launcher shim that
;; just run groov View's war. (It'd probably work for any simple war.)
;;
;; Also, some versions of groov View shipped without a necessary logging jar because it happened to
;; be included in the version of Jetty we use, so I provide that one here too.
;;
;; This is the list of external jars that gets copied into groov's classpath before we launch.
;; NOTE: They're all in the resources/ext directory within Jukebox's classpath. I'm sure there's
;; a way to just scan that directory for jars and copy them out without having to list them
;; explicitly, but I'm in a place w/ bad internet right now and no patience to look it up.
(def external-jars ["jetty-6.1.24.jar" "jetty-util-6.1.24.jar" "servlet-api-5.0.16.jar"
                    "groov-launcher-0.1.0.jar" "log4j-1.2.17.jar"])

;; A log4j2 configuration file that overrides the one built-in to groov releases to add STDOUT logging.
(def override-log4j2-config "log4j2-override.xml")

(defn ^:private is-windows?
  "Figure out whether we're running on Windows. We'll need to know whether to add a '.exe' to the command line."
  []
  (.equalsIgnoreCase "windows" (System/getProperty "os.name")))

(defn ^:private java-executable
  "Figures out the location of the Java executable."
  ^java.io.File []
  (let [exe-name (if (is-windows?) "java.exe" "java")]
    (io/file (str (System/getProperty "java.home") "/bin/" exe-name))))

(defn ^:private unpack-war-file
  "Unzips the given file into the target directory."
  [war-file target-dir]
  (with-open [is (ZipInputStream. (io/input-stream war-file))]
    (loop [entry (.getNextEntry is)]
      (when-not (nil? entry)
        (let [target-file (io/file target-dir (.getName entry))]
          ;; So this is fun: sometimes files come before their directory entries in the war file, so we can't
          ;; be sure the directory's been made yet. Just in case, we'll try and make all parent directories
          ;; before copying a file.
          (if (.isDirectory entry)
            (files/ensure-directory target-file)
            (do (files/ensure-directory (.getParentFile target-file))
                (io/copy is target-file)))
          (recur (.getNextEntry is)))))))

(defn ^:private move-jars
  "Moves everything from the WEB-INF/lib directory within a war-root to lib-dir, returning a list of Files pointing to the moved files."
  [^File war-root ^File lib-dir]
  (let [source-dir (io/file war-root "WEB-INF" "lib")
        files (.listFiles source-dir)]
    (log/info "Moving jars from " (.getAbsolutePath source-dir) " to " (.getAbsolutePath lib-dir) ".")
    (doseq [^File source files]
      (let [target (io/file lib-dir (.getName source))]
        (Files/move (.toPath source) (.toPath target) copy-options)))))

(defn ^:private copy-external-jars
  "Copies Jukebox's bundled external jars out into groov's lib directory."
  [lib-dir]
  (let [lib-dir (io/file lib-dir)]
    (log/info "Copying external jars into" (.getAbsolutePath lib-dir) ".")
    (doseq [jar external-jars]
      (with-open [is (io/input-stream (io/resource (str "ext/" jar)))]
        (io/copy is (io/file lib-dir jar))))))

(defn- disable-security-constraints
  "groovApp.war as shipped has a security constraint in web.xml that requires groov be run under SSL, which
  is annoying for testing purposes. This scrapes through web.xml and deletes that constraint."
  [war-root]
  ;; I can't be bothered to actually parse XML and whatnot. Just read the file line by line, copying each
  ;; line into a temporary file, until we find the one that says "Redirect http to https". The security
  ;; constraint is the last thing in the file, so we can just end it at that line.
  (let [web-xml (io/file war-root "WEB-INF" "web.xml")
        temp (Files/createTempFile (.toPath (.getParentFile web-xml)) "web.xml." "" (into-array FileAttribute []))]
    (log/info "Disabling security constraint in " (.getAbsolutePath web-xml))
    (with-open [in (io/reader web-xml)
                out (PrintWriter. (io/writer (.toFile temp)))]
      (loop [lines (line-seq in)]
        (when-let [line (first lines)]
          (if (string/index-of line "Redirect http to https")
            ;; We can close out the file now.
            (.println out "</web-app>")
            (do
              ;; Just copy out the current line and move to the next
              (.println out line)
              (recur (rest lines)))))))

    ;; Now replace the existing web.xml
    (Files/move temp (.toPath web-xml) copy-options)))

(defn ^:private override-logging
  [war-root]
  (let [log4j2-config (io/file war-root "WEB-INF" "classes" "log4j2.xml")]
    (when (.exists log4j2-config)
      (with-open [is (io/reader (io/resource override-log4j2-config))]
        (io/copy is log4j2-config)))))

(defn ^:private build-classpath
  "Builds a classpath string with the files in lib-dir."
  [lib-dir war-root]
  (let [cp (conj (vec (.listFiles (io/file lib-dir)))
                 (io/file war-root "WEB-INF" "classes"))]
    (string/join ":" cp)))

(defn provision-process
  "Extracts an application and project into the process directory, preparing it to run."
  [process application-file project-file]
  (f/attempt-all [working-dir (files/ensure-directory (:process/working-dir process))
                  library-dir (files/ensure-directory (:process/library-dir process))
                  project-dir (files/ensure-directory (:process/project-dir process))]
                 (do
                   (unpack-war-file application-file working-dir)
                   (move-jars working-dir library-dir)
                   (copy-external-jars library-dir)
                   (disable-security-constraints working-dir)
                   (override-logging working-dir)

                   (when project-file
                     (io/copy project-file (io/file project-dir "project.grv")))

                   process)))

(defn ^:private build-launcher
  ^ProcessBuilder
  [process]
  (-> (ProcessBuilder. [(.getAbsolutePath (java-executable))
                        "-cp" (build-classpath (:process/library-dir process) (:process/working-dir process))
                        (str "-Djava.library.path=" (.getAbsolutePath (:process/library-dir process)))
                        "-Xmx256m"
                        "-Xms256m"
                        (str "-Dgroov.project.dir=" (.getAbsolutePath (:process/project-dir process)))
                        (str "-Dgroov.port=" (str (:process/port process)))
                        "com.opto22.groov.launcher.Launcher"])
      (.directory      (:process/working-dir process))
      (.redirectOutput (:process/stdout process))
      (.redirectError  (:process/stderr process))))

(defn launch-process
  [process]
  (log/infof "Launching groov process %s on port %s." (:process/id process) (:process/port process))
  (let [launcher (build-launcher process)
        pid (.start launcher)]
    (assoc process
           :process/pid pid
           :process/launch-time (Date.)
           :process/state :process/running)))

(defn stop-process
  [process]
  (when-let [pid (:process/pid process)]
    (.destroy pid)
    (dissoc process :process/pid)))

(defn resurrect-process
  "Given just a process id, rebuild the extra structure around it. This is used at system startup
  time after loading processes from the database."
  [process-id processes-root]
  (let [process-root (io/file processes-root (str process-id))
        working (io/file process-root "war-root")
        project (io/file process-root "project")
        library (io/file process-root "lib")
        stdout  (io/file project "stdout.txt")
        stderr  (io/file project "stderr.txt")]
    {:process/id          process-id
     :process/working-dir working
     :process/project-dir project
     :process/library-dir library
     :process/stdout      stdout
     :process/stderr      stderr}))

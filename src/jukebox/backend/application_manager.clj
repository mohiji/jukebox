(ns jukebox.backend.application-manager
  "This feels like a bad idea, but it's here to make a connection between applications in the
  database and those on disk."
  (:require [clojure.java.io :as io]
            [com.stuartsierra.component :as component]
            [failjure.core :as f]
            [jukebox.backend.application :as application]
            [jukebox.backend.db :as db]
            [jukebox.backend.files :as files]))

(defrecord ApplicationManager [application-dir db]
  component/Lifecycle
  (start [component]
    (files/ensure-directory application-dir)
    component)

  (stop [component]
    component))

(defn make-application-manager [application-dir]
  (ApplicationManager. application-dir nil))

(defn import-application
  [app-mgr file-or-path display-name]
  (when-let [app (application/validate-application file-or-path)]
    (when-let [app (db/add-application (:db app-mgr)
                                       (if display-name
                                         (assoc app :application/display-name display-name)
                                         app))]
      (let [app-dir (io/file (:application-dir app-mgr) (str (:application/id app)))]
        (assoc app :application/file (files/copy-to-directory (:application/file app) app-dir))))))

(defn ^:private resolve-file
  [app-mgr app]
  (when app
    (assoc app :application/file (io/file (:application-dir app-mgr)
                                          (str (:application/id app))
                                          (:application/filename app)))))

(defn list-applications
  [app-mgr]
  (map #(resolve-file app-mgr %) (db/list-applications (:db app-mgr))))

(defn ^:private get-app
  "Wrapper function for looking up apps in Jukebox's database, switching on the different type."
  [db type id]
  (condp = type
    :version (db/get-application-version db id)
    :revision (db/get-application-revision db id)
    :id (db/get-application db id)
    (f/fail "Don't know how to look up a groov app with type %s." type)))

(defn get-application
  [app-mgr [query-type query-value]]
  (resolve-file app-mgr (get-app (:db app-mgr) query-type query-value)))

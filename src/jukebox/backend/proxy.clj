(ns jukebox.backend.proxy
  "Really simple HTTP proxy, adapted from https://github.com/FundingCircle/ring-request-proxy.

  I could almost use their stuff as a drop-in, but on groov 3.3 and up we rely on the Host
  field staying intact for CORS requests, and ring-request-proxy overwrites it."
  (:require [clj-http.client :as client]
            [clj-time.coerce :refer [from-date]]
            [clojure.data.json :as json]))

(def ^:private not-found-response {:status 404
                                   :body (json/write-str {:message "Not found"})})

(defn- build-url [host path query-string]
  (let [url (.toString (java.net.URL. (java.net.URL. host) path))]
    (if (not-empty query-string)
      (str url "?" query-string)
      url)))

(defn- handle-not-found [request]
  not-found-response)

(defn- host-from-url [url]
  (when url
    (clojure.string/replace url #"http://" "")))

(defn- clean-cookies
  "The cookies returned by a clj-http request will have an #inst value for their :expires key,
  where ring expects either a String or a joda.time.DateTime instance, so we correct for that
  here.

  There are also a couple of extra keys that ring doesn't like."
  [response]
  (update-in response [:cookies]
             (fn [cookie-map]
               (reduce-kv (fn [m k v]
                            (assoc m k (-> (dissoc v :discard :version)
                                           (update-in [:expires] from-date))))
                          {} cookie-map))))

(defn- create-proxy-fn [handler opts]
  (let [identifier-fn (get opts :identifier-fn identity)
        server-mapping (get opts :host-fn {})]
    (fn [request]
      (let [request-key (identifier-fn request)
            host (server-mapping request-key)
            stripped-headers (dissoc (:headers request) "content-length")
            ;replaced-host-headers (assoc stripped-headers "host" (host-from-url host))
            ]
        (if host
          (-> (select-keys (client/request {:url              (build-url host (:uri request) (:query-string request))
                                            :method           (:request-method request)
                                            :body             (:body request)
                                            :headers          stripped-headers
                                            :throw-exceptions false
                                            :decompress-body  false
                                            :as               :stream})
                           [:status :headers :cookies :body])
              clean-cookies)
          (handler request))))))

(defn proxy-request
  ([opts]
   (proxy-request handle-not-found opts))
  ([handler opts]
   (create-proxy-fn handler opts)))

(ns jukebox.backend.http
  "The web server component!"
  (:require [clojure.string :as string]
            [clojure.pprint :refer [pprint]]
            [clojure.data.json :as json]
            [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [ring.util.response :as response]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [ring.middleware.cookies :refer [wrap-cookies]]
            [ring.middleware.params :refer [wrap-params]]
            [ring.middleware.resource :refer [wrap-resource]]
            [org.httpkit.server :as httpkit]

            [compojure.core :refer :all]
            [compojure.route :as route]

            [jukebox.backend.application-manager :as application-manager]
            [jukebox.backend.pages :as pages]
            [jukebox.backend.process-manager :as process-manager]
            [jukebox.backend.proxy :as proxy])
  (:import [java.util UUID]))

(defn log-request
  "Tiny bit of Ring middleware that logs requests to stdout."
  [handler]
  (fn
    [req]
    (log/info (-> (:request-method req) name string/upper-case) (:server-name req) (:uri req))
    (handler req)))

(defn print-request
  "Mostly for development/debugging, just echos the request back to the client."
  [req]
  {:status 200
   :headers {"Content-Type" "text/plain"}
   :body (with-out-str (pprint req))})

(defn process-lookup
  "Given the server name that came in the request, maps it to the correct groov process, if it's running."
  [process-manager server-name]
  (let [parts (string/split server-name #"\.")
        process-id (first parts)]
    (when-let [process (process-manager/process-for-url process-manager process-id)]
      (when-let [port (:process/port process)]
        (str "http://localhost:" (:process/port process))))))

(defn parse-process-id
  [process-id]
  (try (UUID/fromString process-id)
       (catch Exception e nil)))

(defn start-process
  [process-manager process-id]
  (if-let [process (process-manager/launch-process process-manager (parse-process-id process-id))]
    (do (log/info "Starting process" process-id)
        {:status 303
         :headers {"Location" "/"}
         :body (str "Starting process " process-id)})
    (do (log/warn "Unable to start process" process-id)
        {:status 303
         :headers {"Location" "/"}
         :body (str "Unable to start process " process-id)})))

(defn stop-process
  [process-manager process-id]
  (if-let [process (process-manager/stop-process process-manager (parse-process-id process-id))]
    (do (log/info "Stopping process" process-id)
        {:status 303
         :headers {"Location" "/"}
         :body (str "Stopping process " process-id)})
    (do (log/warn "Unable to stop process" process-id)
        {:status 303
         :headers {"Location" "/"}
         :body (str "Unable to stop process " process-id)})))

(defn delete-process
  [process-manager process-id]
  (if-let [process (process-manager/delete-process process-manager (parse-process-id process-id))]
    (do (log/info "Deleting process" process-id)
        {:status 303
         :headers {"Location" "/"}
         :body (str "Deleting process " process-id)})
    (do (log/warn "Unable to delete process " process-id)
        {:status 303
         :headers {"Location" "/"}
         :body (str "Unable to delete process " process-id)})))

(defn add-process
  [process-manager app-id process-name]
  (if-let [process (process-manager/provision-process process-manager (parse-process-id app-id) nil process-name)]
    (do (log/infof "Created new process '%s' (%s) (app %s)" (:process/display-name process) (:process/id process) (:application/id process))
        {:status 303
         :headers {"Location" "/"}})
    (do (log/warn "Unable to create new process.")
        {:status 303
         :headers {"Location" "/"}})))

(defn json-response
  [thing]
  {:status 200
   :headers {"Content-Type" "application/json; charset=utf-8"
             "Cache-Control" "no-store"}
   :body (json/write-str thing)})

(defn list-processes
  [process-manager]
  ;; I might need to massage this a bit, we'll see.
  (let [processes (or (process-manager/list-processes process-manager) [])]
    (json-response processes)))

(defn list-applications
  [app-mgr]
  (let [apps (or (application-manager/list-applications app-mgr) [])]
    (json-response apps)))

(defn get-applications
  "Returns a map of application-id -> application to give the page renderer a lookup table to work with."
  [application-manager]
  (reduce #(assoc %1 (:application/id %2) %2) {} (application-manager/list-applications application-manager)))

(defn build-router
  "Puts together a Compojure routing thingamajig to handle requests in the ring app.

  Yeah, I know, that's a really opaque comment if you don't already know what I'm talking about."
  [process-manager application-manager]
  (routes
   (GET "/" req (pages/processes-page (process-manager/list-processes process-manager)
                                      (get-applications application-manager)
                                      (select-keys req [:server-name :server-port :scheme])))
   (GET "/dump-request" req (print-request req))
   (POST "/start-process" req (start-process process-manager (get-in req [:params "process-id"])))
   (POST "/stop-process" req (stop-process process-manager (get-in req [:params "process-id"])))
   (POST "/delete-process" req (delete-process process-manager (get-in req [:params "process-id"])))
   (POST "/add-process" req (add-process process-manager (get-in req [:params "app-id"]) (get-in req [:params "process-name"])))
   (GET "/processes.json" req (list-processes process-manager))
   (GET "/applications.json" req (list-applications application-manager))
   (route/not-found "Page not found.")))

(defn ring-app
  [process-manager application-manager]
  (-> (build-router process-manager application-manager)

      ;; Pulls query/form parameters out of the request body or url
      ;; and sticks them in the request map.
      wrap-params

      ;; If request looks like something we need to proxy, do that and skip the above handlers.
      (proxy/proxy-request {:identifier-fn :server-name
                            :host-fn (partial process-lookup process-manager)})

      ;; I don't currently use cookies within Jukebox (there's no authentication, so why?), but we need
      ;; to make sure cookies from proxied requests get handled correctly.
      wrap-cookies

      ;; If the request matches something in the resources/public directory, serve that something up
      ;; and don't go on to the handlers above here.
      (wrap-resource "public")

      ;; If there's not already a content-type header set, try and guess one based on what's in the
      ;; response map at this point.
      wrap-content-type

      ;; Log the request to stdout.
      ;;log-request
      ))

(defrecord HttpServer [port server application-manager process-manager]
  component/Lifecycle
  (start [component]
    (log/infof "Starting the HTTP server on port %d." port)
    (assoc component :server (httpkit/run-server (ring-app process-manager application-manager) {:port port})))

  (stop [component]
    (when server
      (log/infof "Stopping the HTTP server.")
      (server))
    (assoc component :server nil)))

(defn make-http-server
  [port]
  (HttpServer. port nil nil nil))

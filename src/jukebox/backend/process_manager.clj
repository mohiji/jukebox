(ns jukebox.backend.process-manager
  "Component that keeps track of the processes in the system, deals with stopping and starting
  them, etc."
  (:require [clojure.spec.alpha :as s]
            [clojure.java.io :as io]
            [failjure.core :as f]
            [taoensso.timbre :as log]
            [com.stuartsierra.component :as component]

            [jukebox.backend.application-manager :as application-manager]
            [jukebox.backend.db :as db]
            [jukebox.backend.files :as files]
            [jukebox.backend.process :as process])
  (:import [java.util UUID]))

(s/def :process-manager/instances (s/map-of uuid? :system/process))
(s/def :process-manager/url-map   (s/map-of string? uuid?))
(s/def :process-manager/state     (s/keys :req [:process-manager/instances :process-manager/url-map]))

;; The port range we'll run groov instances on.
(def min-port 9000)
(def max-port 9999)

(def proxy-words
  "A list of animal names to use to map URLs to groov instances."
  (with-open [r (io/reader (io/resource "words.txt"))]
    (vec
     (line-seq r))))

(defn gen-name
  "Generates a random-ish name to serve as the thing that maps URLs to the
  instances we're proxying.  It'll return something like 'alligator43'."
  []
  (let [word (rand-nth proxy-words)
        suffix (+ (rand-int 90) 10)]
    (str word suffix)))

(defn ^:private process-agent-validator
  [state]
  (let [valid (s/valid? :process-manager/state state)]
    (when-not valid
      (s/explain :process-manager/state state))
    valid))

(defn ^:private load-processes
  [db process-dir]
  (let [processes    (map #(merge % (process/resurrect-process (:process/id %) process-dir)) (db/list-processes db))
        instance-map (reduce #(assoc %1 (:process/id %2) %2) {} processes)
        url-map      (reduce #(assoc %1 (:process/url-slug %2) (:process/id %2)) {} processes)]
    {:process-manager/instances instance-map
     :process-manager/url-map url-map}))

(defrecord ProcessManager [process-dir db application-manager state]
  component/Lifecycle
  (start [component]
    (f/when-let-ok? [d (files/ensure-directory process-dir)]
      (let [db-processes (load-processes db process-dir)
            state-agent  (agent db-processes)]
        (set-validator! state-agent process-agent-validator)
        (assoc component :state state-agent))))

  (stop [component]
    (doseq [p (vals (:process-manager/instances @state))]
      (when-let [pid (:process/pid p)]
        (.destroy ^java.lang.Process pid)))
    (assoc component :state nil)))

(defn make-process-manager
  [process-dir]
  (ProcessManager. process-dir nil nil nil))

(defn list-processes
  [process-mgr]
  (let [state (:state process-mgr)]
    (vals (:process-manager/instances @state))))

(defn ^:private provision-fn
  [state db process application-file project-file]
  (log/info "Provisioning process" (:process/id process))
  (let [r (process/provision-process process application-file project-file)
        process (assoc process :process/state (if (f/failed? r) :process/provisioning-failed :process/ready))]
    (db/update-process db (:process/id process) process)
    (-> state
        (assoc-in [:process-manager/instances (:process/id process)] process)
        (assoc-in [:process-manager/url-map   (:process/url-slug process)] (:process/id process)))))

(defn provision-process
  [process-mgr application-id project-id description]
  (when-let [app (application-manager/get-application (:application-manager process-mgr)
                                                      [:id application-id])]
    (let [id (UUID/randomUUID)
          base-dir (io/file (:process-dir process-mgr) (str id))
          process {:process/id id
                   :process/display-name description
                   :process/url-slug (gen-name)
                   :process/working-dir (io/file base-dir "war-root")
                   :process/project-dir (io/file base-dir "project")
                   :process/library-dir (io/file base-dir "lib")
                   :process/stdout      (io/file base-dir "project" "stdout.txt")
                   :process/stderr      (io/file base-dir "project" "stderr.txt")
                   :process/state       :process/provisioning
                   :application/id      application-id}]
      (db/add-process (:db process-mgr) process)
      (send-off (:state process-mgr) provision-fn (:db process-mgr) process (:application/file app) nil)
      process)))

(defn get-process
  [process-mgr process-id]
  (get-in @(:state process-mgr) [:process-manager/instances process-id]))

(defn ^:private get-free-port
  "Given a set of ports, find a free one somewhere in the range [min-port, max-port)"
  [ports min-port max-port]
  (loop [port (+ min-port (rand-int (- max-port min-port)))]
    (if (contains? ports port)
      (recur (+ min-port (rand-int (- max-port min-port))))
      port)))

(defn ^:private ports-in-use
  [processes]
  (set (map :process/port processes)))

(defn ^:private launch-fn
  [state process]
  (let [port (get-free-port (ports-in-use (vals (:process-manager/instances state))) min-port max-port)
        process (process/launch-process (assoc process :process/port port))]
    (log/info "launched process" (with-out-str (clojure.pprint/pprint process)))
    (assoc-in state [:process-manager/instances (:process/id process)] process)))

(defn launch-process
  [process-mgr process-id]
  (when-let [process (get-process process-mgr process-id)]
    (send-off (:state process-mgr) launch-fn process)
    process))

(defn ^:private stop-fn
  [state process]
  (.destroy ^java.lang.Process (:process/pid process))
  (let [process (-> (dissoc process :process/pid :process/port :process/launch-time)
                    (assoc :process/state :process/ready))]
    (assoc-in state [:process-manager/instances (:process/id process)] process)))

(defn stop-process
  [process-mgr process-id]
  (when-let [process (get-process process-mgr process-id)]
    (when (:process/pid process)
      (send-off (:state process-mgr) stop-fn process))
    process))

(defn delete-fn
  [state process-id]
  ;; TODO This doesn't clear out the directories on disk yet.
  (let [slug (get-in state [:process-manager/instances process-id :process/url-slug])]
    (-> (update-in state [:process-manager/instances] #(dissoc %1 process-id))
        (update-in [:process-manager/url-map] #(dissoc %1 slug)))))

(defn delete-process
  [process-mgr process-id]
  (when-let [process (stop-process process-mgr process-id)]
    (db/delete-process (:db process-mgr) process-id)
    (send-off (:state process-mgr) delete-fn process-id)))

(defn process-for-url
  [process-mgr url-slug]
  (when-let [process-id (get-in @(:state process-mgr) [:process-manager/url-map url-slug])]
    (get-process process-mgr process-id)))

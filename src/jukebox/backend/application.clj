(ns jukebox.backend.application
  "Code for validating and unpacking groov application war files."
  (:require [clojure.java.io :as io]
            [clojure.string :as string]
            [failjure.core :as f]
            [jukebox.backend.files :as files])
  (:import [java.io File]
           [java.time Instant ZonedDateTime ZoneId]
           [java.time.format DateTimeFormatter DateTimeParseException]
           [java.util Date Properties]
           [java.util.zip ZipEntry ZipFile]))

(defn ^:private file?
  [file-or-path]
  (if-let [f (io/file file-or-path)]
    (if (.isFile f)
      f
      (f/fail "'%s' is not a file." file-or-path))
    (f/fail "Could not create a File instance from '%s'." file-or-path)))

(defn ^:private properties-file
  "Opens the given file as a zip file, pulls out the groov.properties file, and returns
  a Properties instance with its contents, if it can. Otherwise returns a failjure error."
  [^java.io.File f]
  (f/if-let-ok? [zip (f/try* (ZipFile. f))]
    (with-open [zip zip]
      (if-let [entry (.getEntry zip "WEB-INF/classes/groov.properties")]
        (let [props (Properties.)
              is    (.getInputStream zip entry)]
          (.load props is)
          props)
        (f/fail "Missing groov.properties in the archive file.")))
    (f/fail "Fail '%s' was not a zip file." (.getAbsolutePath f))))

(def ^:private revision-matcher
  "A regular expression that matches groov's revision number strings."
  #"\(r?([0-9]+)\)")

(defn parse-version
  "Groov's version strings look like 'R1.0d (16364)' or 'R2.1b (r19768)'. This function breaks it up
  into a version and a revision number"
  [version-string]
  (try
    (let [[version rev-string] (string/split version-string #" ")
          revision (Integer/parseInt (get (re-matches revision-matcher rev-string) 1))]
      {:application/version version
       :application/revision revision})
    (catch Exception e
      (f/fail "Unable to determine the application's version or revision from string '%s'." version-string))))

(def ^:private newer-date-formatter
  "A date formatter for newer releases of groov."
  (DateTimeFormatter/ofPattern "MMMM d, yyyy h:mm:ss a z"))

(def ^:private older-date-formatter
  "A date formatter for older releases of groov."
  (.withZone
   (DateTimeFormatter/ofPattern "MMMM d, yyyy h:mm a")
   (ZoneId/of "America/Los_Angeles")))

(defn parse-date
  "We store build dates as strings, but I want them back as real time values."
  [date-string]
  ;; At some point, and I have no idea why or when, we changed our timestamp
  ;; to include seconds and the time zone it's in, so we have to try both.
  (try (Date/from (.toInstant (ZonedDateTime/parse date-string newer-date-formatter)))
       (catch DateTimeParseException _
         (try (Date/from (.toInstant (ZonedDateTime/parse date-string older-date-formatter)))
              (catch DateTimeParseException _
                (f/fail "Unable to parse build date %s." date-string))))))

(defn parse-properties
  "Given a Properties instance from a groovApp.war file, returns a map of the app's version, build
  date, and if available, tech preview value."
  [^java.util.Properties props]
  (f/attempt-all [version (parse-version (get props "version"))
                  build-date (parse-date (get props "formattedDate"))]
                 (merge version {:application/build-date build-date
                                 :application/tech-preview (get props "techPreview")})
                 (f/when-failed [e]
                   (f/message e))))

(defn validate-application
  "Given a java.io.File or a path to a file, verify whether it's a valid groovApp.war file.
  If it is, returns a map of properties about it. Otherwise returns a failjure error."
  [file-or-path]
  (f/ok-> (file? file-or-path)
          properties-file
          parse-properties
          (assoc :application/file (io/file file-or-path))
          (assoc :application/fingerprint (files/print-hex (files/md5-sum (io/file file-or-path))))))

(ns jukebox.backend.pages
  "Code for rendering the two simple pages I support so far."
  (:require [hiccup.page :refer [html5]]))

(defn header [page-title]
  [:head
   [:meta {:charset "UTF-8"}]
   [:meta {:name "viewport"
           :content "width=device-width, initial-scale=1.0, maximum-scale=1.0"}]
   [:title page-title]
   [:link {:rel "stylesheet" :href "css/bootstrap.min.css"}]
   [:link {:rel "stylesheet" :href "css/bootstrap-theme.min.css"}]
   [:link {:rel "stylesheet" :href "css/font-awesome.min.css"}]
   [:link {:rel "stylesheet" :href "css/jukebox.css"}]])

(defn clean-port
  "Returns a string version of the port, if necessary. Meaning, if the scheme is http and the port is 80, we'll return
  nil, otherwise we'll return :80, and so on for https."
  [server-info]
  (cond (and (= 80 (:server-port server-info)) (= :http (:scheme server-info))) nil
        (and (= 443 (:server-port server-info)) (= :https (:scheme server-info))) nil
        :else (str ":" (:server-port server-info))))

(defn groov-link
  [process server-info]
  (let [display-url (str(:process/url-slug process) "." (:server-name server-info) (clean-port server-info))
        full-url (str (name (:scheme server-info)) "://" display-url)]
    (if (= :process/running (:process/state process))
      [:a {:href full-url
           :target "_blank"}
       display-url]
      [:p display-url])))

(defn stop-start-button
  [process]
  (condp = (:process/state process)
    :process/ready (list [:form.process-control-form {:action "/start-process" :method "post"}
                          [:input {:type "hidden" :name "process-id" :value (str (:process/id process))}]
                          [:button {:type "submit" :class "btn btn-default btn-xs"} "Start"]]
                         [:form.process-control-form {:action "/delete-process" :method "post"}
                          [:input {:type "hidden" :name "process-id" :value (str (:process/id process))}]
                          [:button {:type "submit" :class "btn btn-danger btn-xs"} "Delete"]])
    :process/running [:form.process-control-form {:action "/stop-process" :method "post"}
                      [:input {:type "hidden" :name "process-id" :value (str (:process/id process))}]
                      [:button {:type "submit" :class "btn btn-default btn-xs"} "Stop"]]
    [:p "Unhandled process state"]))

(defn render-app
  "Renders a nice looking version string for an application."
  [app]
  (str (:application/version app) " (" (:application/revision app) ")"))

(defn process-list
  [processes applications server-info]
  [:table.table
   [:thead
    [:tr
     [:th "Name"]
     [:th "URL"]
     [:th [:i "groov"] " Version"]
     [:th "State"]
     [:th "&nbsp;"]]]
   [:tbody
    (for [p processes]
      [:tr
       [:td (:process/display-name p)]
       [:td (groov-link p server-info)]
       [:td (render-app  (get applications (:application/id p)))]
       [:td (:process/state p)]
       [:td (stop-start-button p)]])]])

(defn processes-panel
  [processes applications server-info]
  [:div {:class "panel panel-default"}
   [:div.panel-heading
    [:h3.panel-title "Processes"]]
   (process-list processes applications server-info)])

(defn application-name
  [application]
  [:span [:i "groov"] (str " View " (:application/version application) " (" (:application/revision application) ")")])

(defn application-list
  [applications]
  [:select.form-control {:name "app-id" :id "app-id"}
   (for [app (sort-by :application/revision (vals applications))]
     [:option {:value (str (:application/id app))} (application-name app)])])

(defn add-process-panel
  [applications]
  [:div {:class "panel panel-default"}
   [:div.panel-heading
    [:h3.panel-title "Add Process"]]
   [:div.panel-body
    [:form {:class "form" :action "/add-process" :method "post"}
     [:div.form-group
      [:label.control-label {:for "process-name"} "Process Name"]
      [:input.form-control {:name "process-name" :id "process-name" :type "text"}]]

     [:div.form-group
      [:label.control-label {:for "app-id"} "Application Version"]
      (application-list applications)]

     [:div.form-group
      [:button {:type "submit" :class "btn btn-primary"} "Add Application"]]]]])

(defn processes-page
  [processes applications server-info]
  (html5
   {:lang "en"}
   (header "Process - Jukebox")
   [:body
    [:div.container
     [:div {:class "header clearfix"}
      [:h3.text-muted "Jukebox"]]

     [:div.row
      [:div {:class "col-xs-12 col-sm-8"}
       (processes-panel processes applications server-info)]
      [:div {:class "col-xs-12 col-sm-4"}
       (add-process-panel applications)]]]]))

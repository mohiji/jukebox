(ns jukebox.backend.dev
  "Namespace that's only used at development time. This is pretty much my scratchpad."
  (:require [reloaded.repl :refer [system init start stop go reset reset-all clear]]
            [clojure.pprint :refer [pprint]]
            [clojure.java.io :as io]
            [clojure.string :as string]
            [jukebox.backend.application :as application]
            [jukebox.backend.application-manager :as application-manager]
            [jukebox.backend.config :refer [config]]
            [jukebox.backend.db :as db]
            [jukebox.backend.process-manager :as process-manager]
            [jukebox.backend.system :as system]))

(reloaded.repl/set-init! #(system/make-system config))

;; Simple wrapper functions because I get tired of picking components out of the system map at dev time.

(defn get-application [app-ident]
  (application-manager/get-application (:application-manager system) app-ident))

(defn import-application [path description]
  (application-manager/import-application (:application-manager system) path description))

(defn list-applications []
  (application-manager/list-applications (:application-manager system)))

(defn list-processes []
  (process-manager/list-processes (:process-manager system)))

(defn delete-process [process-id]
  (process-manager/delete-process (:process-manager system) process-id))

(defn provision-process [app-id project-id process-name]
  (process-manager/provision-process (:process-manager system) app-id project-id process-name))

(defn launch-process [process-id]
  (process-manager/launch-process (:process-manager system) process-id))

(defn stop-process [process-id]
  (process-manager/stop-process (:process-manager system) process-id))

(defn import-applications
  "Import all applications in the named dir."
  [app-dir]
  (let [app-dir (io/file app-dir)]
    (reduce (fn [results file]
              (if (and (.isFile file)
                       (or (string/ends-with? (.getName file) "bin")
                           (string/ends-with? (.getName file) "war")))
                (do (println "Importing file " (.getName file))
                    (conj results (import-application file (.getName file))))
                results))
            []
            (.listFiles app-dir))))

(ns jukebox.backend.core
  (:require [com.stuartsierra.component :as component]
            [jukebox.backend.config :refer [config]]
            [jukebox.backend.system :refer [make-system]])
  (:gen-class))

(defn -main
  "Starts up Jukebox!"
  [& args]
  (let [system (make-system config)]
    (component/start system)))

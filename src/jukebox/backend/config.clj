(ns jukebox.backend.config
  "Central place to store configuration. This mostly gets populated from the environment
  at launch time."
  (:require [clojure.java.io :as io]
            [environ.core :refer [env]]))

(defn get-port []
  (if-let [port-str (:jukebox-http-port env)]
    (try (Integer/parseInt port-str)
         (catch Exception e 4000))
    4000))

(def config
  (let [base-dir (get env :jukebox-data-dir (System/getProperty "user.dir"))
        data-dir (io/file base-dir)]
    {:config/base-dir        data-dir
     :config/db              (io/file base-dir "jukebox.db")
     :config/application-dir (io/file data-dir "applications")
     :config/project-dir     (io/file data-dir "projects")
     :config/process-dir     (io/file data-dir "processes")
     :config/http-port       (get-port)}))

(ns jukebox.backend.files
  "Helper functions for copying files around."
  (:require [clojure.java.io :as io]
            [failjure.core :as f])
  (:import [java.nio.file Files]
           [java.security MessageDigest]))

(defn ensure-directory
  [^java.io.File dir]
  (cond (.isDirectory dir) dir
        (.exists dir) (f/fail "'%s' exists but is not a directory." (.getAbsolutePath dir))
        :else (if (.mkdirs dir)
                dir
                (f/fail "Failed to create directory '%s'." (.getAbsolutePath dir)))))

(defn copy-to-directory
  [^java.io.File source ^java.io.File destination]
  (f/when-let-ok? [d (ensure-directory destination)]
    (let [target (io/file destination (.getName source))]
      (io/copy source (io/file destination (.getName source)))
      target)))

(defn ^:private hash-sum
  [^java.io.File f ^java.security.MessageDigest md]
  (.update md (Files/readAllBytes (.toPath f)))
  (.digest md))

(defn sha1-sum
  [^java.io.File f]
  (hash-sum f (MessageDigest/getInstance "SHA-1")))

(defn sha256-sum
  [^java.io.File f]
  (hash-sum f (MessageDigest/getInstance "SHA-256")))

(defn md5-sum
  [^java.io.File f]
  (hash-sum f (MessageDigest/getInstance "MD5")))

(defn print-hex
  "Prints out the given byte array as a hex string."
  [b]
  (let [builder (StringBuilder.)]
    (doseq [b (seq b)]
      (.append builder (format "%02x" b)))
    (.toString builder)))

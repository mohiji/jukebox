(ns jukebox.backend.system
  "Pulls together the various components in the project into a system we can stop and start."
  (:require [com.stuartsierra.component :as component]
            [jukebox.backend.application-manager :as application-manager]
            [jukebox.backend.db :as db]
            [jukebox.backend.http :as http]
            [jukebox.backend.process-manager :as process-manager]

            ;; I don't directly use the specs here, but I need to make sure they get pulled in somewhere.
            jukebox.common.specs))

(defn make-system
  "Builds the system of components that runs this whole shebang."
  [config]
  (component/system-map
   :db (db/make-db-component (:config/db config))
   :application-manager (component/using
                         (application-manager/make-application-manager (:config/application-dir config))
                         [:db])
   :process-manager (component/using
                     (process-manager/make-process-manager (:config/process-dir config))
                     [:db :application-manager])
   :http (component/using
          (http/make-http-server (:config/http-port config))
          [:application-manager :process-manager])))

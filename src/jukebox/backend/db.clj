(ns jukebox.backend.db
  "Database access."
  (:require [clojure.java.jdbc :as jdbc]
            [failjure.core :as f]
            [com.stuartsierra.component :as component]
            ragtime.jdbc
            ragtime.repl

            [jukebox.backend.files :as files])
  (:import [java.util Date UUID]))

(defn ^:private inst->timestamp
  [inst]
  (int (/ (.getTime inst) 1000)))

(defn ^:private timestamp->inst
  [ts]
  (Date. (* ts 1000)))

(defn ^:private row->application
  [row]
  {:application/id           (UUID/fromString (:id row))
   :application/filename     (:filename row)
   :application/display-name (:display_name row)
   :application/version      (:version row)
   :application/revision     (:revision row)
   :application/tech-preview (:tech_preview row)
   :application/build-date   (timestamp->inst (:build_date row))
   :application/fingerprint  (:fingerprint row)})

(defn ^:private application->row
  [app]
  (let [filename (or (:application/filename app) (.getName (:application/file app)))
        display-name (get app :application/display-name filename)]
    {:id           (.toString (:application/id app))
     :filename     filename
     :display_name display-name
     :version      (:application/version app)
     :revision     (:application/revision app)
     :tech_preview (:application/tech-preview app)
     :build_date   (inst->timestamp (:application/build-date app))
     :fingerprint  (:application/fingerprint app)}))

(defn init-db
  "Brings up a database from scratch."
  [db]
  (let [ragtime-config {:datastore  (ragtime.jdbc/sql-database db)
                        :migrations (ragtime.jdbc/load-resources "migrations")}]
    (ragtime.repl/migrate ragtime-config)))

(defn list-applications
  [db]
  (jdbc/query db ["SELECT * FROM applications"] {:row-fn row->application}))

(defn add-application
  "Adds an application to the given database. db can either be a database spec or a connection
  provided as {:connection conn}. app is an application record as described in janet.specs

  Returns the same record with an :application/id attached"
  [db app]
  (let [app-id (UUID/randomUUID)
        app (assoc app :application/id app-id)]
    (jdbc/insert! db :applications (application->row app))
    app))

(defn get-application
  [db app-id]
  (first (jdbc/query db
                     ["SELECT * FROM applications WHERE id=?" (str app-id)]
                     {:row-fn row->application})))

(defn get-application-version
  [db app-version]
  (first (jdbc/query db
                     ["SELECT * FROM applications WHERE version=? ORDER BY rowid DESC LIMIT 1" app-version]
                     {:row-fn row->application})))

(defn get-application-revision
  [db app-revision]
  (first (jdbc/query db
                     ["SELECT * FROM applications WHERE revision=? ORDER BY rowid DESC LIMIT 1" app-revision]
                     {:row-fn row->application})))

(defn update-application
  [db app-id props]
  (jdbc/with-db-transaction [db db]
    (let [existing (get-application db app-id)]
      (when existing
        (jdbc/update! db
                      :applications
                      (application->row (merge existing props))
                      ["id=?" app-id])))))

(defn delete-application
  [db app-id]
  (jdbc/delete! db
                :applications
                ["id=?" app-id]))


(defn ^:private process->row
  [process]
  {:id              (str (:process/id process))
   :application_id  (str (:application/id process))
   :url_slug        (:process/url-slug process)
   :display_name    (:process/display-name process)
   :provision_state (condp = (:process/state process)
                      :process/provisioning-failed "failed"
                      :process/provisioning "none"
                      "ready")})

(defn ^:private row->process
  [row]
  {:process/id           (UUID/fromString (:id row))
   :application/id       (UUID/fromString (:application_id row))
   :process/url-slug     (:url_slug row)
   :process/display-name (:display_name row)
   :process/state        (condp = (:provision_state row)
                           "ready"  :process/ready
                           "failed" :process/provisioning-failed
                           "none"   :process/provisioning)})

(defn get-process
  [db process-id]
  (first (jdbc/query db
                     ["SelECT * FROM processes WHERE id=?" (str process-id)]
                     {:row-fn row->process})))

(defn add-process
  [db process]
  (jdbc/insert! db :processes (process->row process))
  process)

(defn update-process
  [db process-id props]
  (jdbc/with-db-transaction [db db]
    (let [existing (get-process db process-id)]
      (when existing
        (jdbc/update! db
                      :processes
                      (process->row (merge existing props))
                      ["id=?" (str process-id)])))))

(defn delete-process
  [db process-id]
  (jdbc/delete! db :processes ["id=?" (str process-id)]))

(defn list-processes
  [db]
  (jdbc/query db ["SELECT * FROM processes"] {:row-fn row->process}))

(defrecord DatabaseComponent [db-file connection]
  component/Lifecycle
  (start [component]
    (f/when-let-ok? [base-dir (files/ensure-directory (.getParentFile db-file))]
      (let [db-config {:dbtype "sqlite"
                       :dbname (.getAbsolutePath db-file)}]
        (when-not (.exists db-file)
          (init-db db-config))
        (assoc component :connection (jdbc/get-connection db-config)))))

  (stop [component]
    (when-let [conn (:connection component)]
      (.close conn))
    (assoc component :connection nil)))

(defn make-db-component
  [db-file]
  (DatabaseComponent. db-file nil))

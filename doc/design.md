# Jukebox Design Notes

Jukebox is built to launch any version of groov View quickly and easily because I got tired of juggling installations on my groov Boxes, etc. Ultimately, I want to be able to:

- Fire up any version of groov via a web interface.
- Let anyone else at Opto22 do the same.
- Automatically import new builds of groov into Jukebox.
- Allow tests to launch a groov instance, run against it, and shut it back down again.

## The Data directory and Database

Jukebox needs a working directory to store all it's stuff in. It picks it up from the environment at launch time: just set an environment variable named `JUKEBOX_DATA_DIR` pointing at the place where you want it to store things before launch.

Within that data directory you'll see the following get created:

- `jukebox.db` - A SQLite database storing metadata about `applications`, `projects`, and `processes`.
- `applications` - Directory where Jukebox stores all of the applications that get imported into it. Each application will be stored under another directory within here that matches its UUID in the database.
- `projects` - Same thing as applications, but for project files.
- `processes` - Working directories for all of the groov instances that've been launched.

## Applications

Individual copies of groovApp.war are called `applications` within Jukebox. The goal here is to use the exact files that come out of our Jenkins builds. They're pristine, signed, etc. Jukebox will make some small modifications to them at launch time (detailed later), but the files it stores are unchanged.

When an `application` is imported into Jukebox, it:

1. Verifies that it's a zip file
2. Pulls out the groov.properties file from within it and parses out the groov version, build revision number, and build date.

I assume if I can do those two things it's a good `groovApp.war`. I generate a random id for the application (just `java.util.UUID/randomUUID`) and store the information I parsed about the application, its filename, and a fingerprint (currently a SHA1 sum) for the application in the database. I store the file itself under the `applications` directory in a sub-directory with the same name as its database id.

## Projects

These aren't implemented yet, but the general idea is the same as `applications`. I'll allow people to import either *groov* backups or raw project databases, pull out the schema version and *groov* version it expects, and store them unchanged under `projects/<project-uuid>`.

## Processes

`Processes` are the actual *groov* instances we're running. Each `process` gets launched with an application id, a description, and optionally a project id.

### Provisioning a process

When a client asks Jukebox to set up a *groov* instance, it will:

1. Assign a random id for the process (another UUID)
2. Assign a url slug for it (more on this later)
3. Create a working directory tree for the process under `JUKEBOX_DATA_DIR/processes/<process-id>`
  + `lib` - Jar files go here
  + `war-root`  - The application will be extracted here
  + `project` - Where the running *groov* stores its files.
4. Unpack the application war file into the `war-root` directory.
5. Move the jars from `war-root/WEB-INF/lib` to the `lib` directory.
6. Copy a few more jars into that `lib` directory:
  + The jars necessary to run Jetty.
  + A copy of the older log4j jar. (Some versions of *groov* shipped
without this accidentally.)
  + A small jar that launches *groov* without needing a fully configured servlet container.
7. Copies a log4j2 configuration file into `war-root/WEB-INF/classes` that'll override *groov*'s logging configuration to log to files as well as the normal logging database.
8. Edit `war-root/WEB-INF/web.xml` to disable the SSL requirement.

Once that's done, it's ready to launch.

### Launching a process

Given a provisioned process, Jukebox will:

1. Assign a random port in the 9000-9999 range.
2. Figure out what Java was used to launch Jukebox itself.
3. Build up a command line that includes all of the libraries in `processes/<process-id>/lib`.
4. Launch the process.

Once that's done, we have a *groov* running on the randomly selected port! Mission accomplished!

## The HTTP Proxy

Well, almost accomplished. As it turns out, browsers don't include port numbers when they decide what sites cookies belong to. So if you have *groov* instances running on `http://localhost:9001` and `http://localhost:9002`, signing into one of them will sign you out of the other. You can get around that for awhile by using multiple browsers, and incognito sessions, but eventually you're going to get stuck and not be able to be signed in to all of your instances at once.

So, Jukebox also includes an HTTP proxy. Each process is assigned a URL slug at provisioning time that'll be persistent across launches. If Jukebox is running as `jukebox.opto22.com`, each running process will be available as `<process-url-slug>.jukebox.opto22.com`, without worrying about port numbers.

Doing this requires some help from whoever runs your DNS. You need two entries:

- An A record pointing to the Jukebox instance's IP address, e.g. mapping jukebox.opto22.com to 10.192.57.111.
- A CNAME wildcard record pointing to your jukebox A record: e.g. *.jukebox.opto22.com pointing to jukebox.opto22.com.
